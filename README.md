# PG operations

Ansible playbook to launch operations on postgreSQL bases.

## Requirements

* You need this role to use this playbook : [PGSQL ops](https://gitlab.mim-libre.fr/infrabricks/ansible/roles/pgsql-ops)
* Ansible >= 4

## OS

* Debian

## Playbook Example

An example of playbook

```
- name: Pgsql ops
  hosts: all
  vars_files:
    - vars/main.yml
    - vars/secret.yml
  tasks:
    - name: Include PGSQL ops role
      ansible.builtin.include_role:
        name: pgsql_ops
```

## Author Information

* [Stéphane Paillet](mailto:spaillet@ethicsys.fr)
